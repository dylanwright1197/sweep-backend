package lancers.sweep.Utils;


import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class DateValidator {

    public Boolean validateDates(String startDate, String endDate) {
        DateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT.value);
        sdf.setLenient(false);
        try {
            sdf.parse(startDate);
            sdf.parse(endDate);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
