package lancers.sweep.Utils;

import lancers.sweep.model.Role;
import lancers.sweep.model.User;
import lancers.sweep.repository.RoleRepository;
import lancers.sweep.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DataGenerator implements CommandLineRunner {



        @Autowired
        UserRepository userRepository;


        @Autowired
        RoleRepository roleRepository;

        public DataGenerator() {
        }

        @Override
        public void run(String... args) throws Exception {

            userRepository.deleteAll();
            roleRepository.deleteAll();

            createRoles();
            createUsers();
        }

        private void createRoles() {
            Role role = new Role("1", "ADMIN");
            Role role1 = new Role("2", "USER");
            Role role2 = new Role("3", "OTHER_USER");

            roleRepository.save(role);
            roleRepository.save(role1);
            roleRepository.save(role2);
        }

        private void createUsers() {

            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            String adminPassword = "lancer1888";
            String userPassword = "user";

            String adminHashedPassword = passwordEncoder.encode(adminPassword);
            String userHashedPassword = passwordEncoder.encode(userPassword);

            User admin = new User("sweep-system-admin",  adminHashedPassword, "1", Constants.ACTIVE.value);
            User user = new User("sweep-user",  userHashedPassword, "2", Constants.ACTIVE.value);

            userRepository.save(admin);
            userRepository.save(user);

        }



    }

