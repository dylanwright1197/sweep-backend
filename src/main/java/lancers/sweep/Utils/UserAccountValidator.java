package lancers.sweep.Utils;

import lancers.sweep.model.User;
import lancers.sweep.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserAccountValidator {

    @Autowired
    UserRepository userRepository;

    public boolean userAccountIsValid(User user) {
        //Check for duplicate usernames
        User u = userRepository.findByUsername(user.getUsername());
        return u == null;
    }

}
