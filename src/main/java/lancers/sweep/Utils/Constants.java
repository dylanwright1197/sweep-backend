package lancers.sweep.Utils;

public enum Constants {

    DATE_FORMAT("yyyy-MM-dd"),
    ACTIVE("ACTIVE");

    public final String value;

    Constants(String value) {
        this.value = value;
    }
}
