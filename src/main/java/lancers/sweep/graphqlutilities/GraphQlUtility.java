package lancers.sweep.graphql_utilities;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import lancers.sweep.dataFetchers.CurrentDayFixtureFetcher;
import lancers.sweep.dataFetchers.FixtureFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

import static graphql.GraphQL.newGraphQL;
import static graphql.schema.idl.RuntimeWiring.newRuntimeWiring;

@Component
public class GraphQlUtility {

    @Value("classpath:schemas.graphqls")
    private Resource schemaResource;
    private final CurrentDayFixtureFetcher currentDayFixtureFetcher;
    private final FixtureFetcher fixtureFetcher;

    @Autowired
    GraphQlUtility(CurrentDayFixtureFetcher currentDayFixtureFetcher, FixtureFetcher fixtureFetcher) {
        this.currentDayFixtureFetcher = currentDayFixtureFetcher;
        this.fixtureFetcher = fixtureFetcher;

    }

    @PostConstruct
    public GraphQL createGraphQlObject() throws IOException {
        File schemas = schemaResource.getFile();
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemas);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        return  newGraphQL(schema).build();
    }

    private RuntimeWiring buildRuntimeWiring(){
        return newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("currentDayFixtureQuery", currentDayFixtureFetcher)
                        .dataFetcher("fixtureQuery", fixtureFetcher)
                )
                .build();
    }
}

