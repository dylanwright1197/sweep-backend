package lancers.sweep.api;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class Api {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private HashMap<String, String> dates = new HashMap<>();

    @Autowired
    private Environment env;


    public String getCurrentDayFixtures() throws ParseException, IOException {

        setCurrentDayFixtures();
        return getFixtures(dates.get("startDate"), dates.get("endDate"));

    }


    public String getFixtures(String startDate, String endDate) throws IOException {

        String baseUrl = env.getProperty("api.base.url");
        String apiKey = env.getProperty("api.key");

        URL url = new URL(baseUrl + "/?" +
                "action=get_events" +
                "&from=" + startDate +
                "&to=" + endDate +
                "&league_id=148" +
                "&APIkey=" + apiKey);

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();


        return content.toString();
    }

    private Map<String, String> setCurrentDayFixtures() throws ParseException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);

        String startDate = simpleDateFormat.format(new Date());

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(startDate));
        c.add(Calendar.DATE, 1);
        String endDate = sdf.format(c.getTime());

        dates.put("startDate", startDate);
        dates.put("endDate", endDate);

        return dates;
    }
}
