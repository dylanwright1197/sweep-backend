package lancers.sweep.controller;

import com.google.gson.Gson;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lancers.sweep.model.Leaderboard;
import lancers.sweep.model.League;
import lancers.sweep.model.User;
import lancers.sweep.service.LeagueService;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.MBeanServerNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class LeagueController {

    @Autowired
    private LeagueService leagueService;


    /** Required fields in request body:
     *
     * leagueName: String
     * competition: String
     * users: User[] - list should contain a single user for this request mapping
     *
     * */
    @RequestMapping(value = "/league", method = RequestMethod.POST)
    public String createLeague(@RequestBody String league) {
        // Parse request body to League object
        League leagueObj = new Gson().fromJson(league, League.class);

        if (validateLeagueName(leagueObj.getLeagueName())) {

            // Create unique string id based on timestamp value
            leagueObj.setLeagueId(getUniqueIdFromTimestamp());

            // Initialise League
            leagueObj.setLeaderBoard(initialiseLeaderBoard(leagueObj));
            leagueObj.setAdmin(leagueObj.getUsers().get(0).getUsername());

            // Create League
            leagueService.createLeague(leagueObj);

            return "League Created: " + leagueObj.getLeagueName() + " (" + leagueObj.getLeagueId() + ")";
        }
        return "This League Name has been taken!";
    }

    @RequestMapping(value = "/league/{leagueName}", method = RequestMethod.GET)
    public League getLeagueByName(@PathVariable String leagueName) throws HttpException {


        League requiredLeague = leagueService.getLeagueByName(leagueName);

        if (requiredLeague == null) {
            throw new HttpException("League name does not exist: " + leagueName);
        }
        return requiredLeague;
    }

    private Leaderboard initialiseLeaderBoard(League league) {
        Leaderboard leaderboard = new Leaderboard();
        Map<String, Integer> initialScores = new HashMap<>();

        // Give each user a default score of 0
        for (User u: league.getUsers()) {
            initialScores.put(u.getUsername(), 0);
        }

        // Set initial user(s) score to the newly created league
        leaderboard.setUserScores(initialScores);

        // Return the initialised leaderboard
        return leaderboard;
    }

    private String getUniqueIdFromTimestamp() {
        Calendar calendar = Calendar.getInstance();
        long uniqueTimestamp = calendar.getTimeInMillis();
        return String.valueOf(uniqueTimestamp);
    }

    private boolean validateLeagueName(String newLeagueName) {
        List<League> allLeagues = leagueService.getAllLeagues();

        for (League league: allLeagues) {
           if (league.getLeagueName().equals(newLeagueName)) {
               return false;
           }
        }
        return true;
    }


}
