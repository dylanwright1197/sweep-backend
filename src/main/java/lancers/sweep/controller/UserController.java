package lancers.sweep.controller;


import com.google.gson.Gson;
import lancers.sweep.Utils.Constants;
import lancers.sweep.Utils.UserAccountValidator;
import lancers.sweep.model.User;
import lancers.sweep.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {


    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountValidator userAccountValidator;


    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
    public List<User> getAllUsers() {

        return userService.getAllUsers();


    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String createUser(@RequestBody String user) {
        User gson = new Gson().fromJson(user, User.class);

        if (gson.getUsername() != null || gson.getPassword() != null) {

            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            boolean userIsValid = userAccountValidator.userAccountIsValid(gson);

            if (userIsValid) {
                String userHashedPassword = passwordEncoder.encode(gson.getPassword());
                User userAccount = new User(gson.getUsername(), userHashedPassword, gson.getRoleId(), Constants.ACTIVE.value);


                userService.createUser(userAccount);

                return "User: " + gson.getUsername() + " created";
            }
            return "Username not valid!";

        }
        return "Username and Password must not be null!";
    }
}
