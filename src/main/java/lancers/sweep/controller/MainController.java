package lancers.sweep.controller;


import com.google.gson.Gson;
import graphql.ExecutionResult;
import graphql.GraphQL;
import lancers.sweep.Utils.DateValidator;
import lancers.sweep.api.Api;
import lancers.sweep.graphql_utilities.GraphQlUtility;
import lancers.sweep.model.Fixture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
@RestController
public class MainController {

    private GraphQL graphQL;

    @Autowired
    Api api;

    @Autowired
    private DateValidator dateValidator;

    @Autowired
    MainController(GraphQlUtility graphQlUtility) throws IOException {
        graphQL = graphQlUtility.createGraphQlObject();
    }

    @PostMapping(value = "/graphql")
    public ResponseEntity query(@RequestBody String query){
        ExecutionResult result = graphQL.execute(query);
        return ResponseEntity.ok(result.getData());
    }

    @GetMapping(value = "/getCurrentDayFixtures")
    public Fixture[] getCurrentDayFixtures() throws IOException, ParseException {

        String jsonStr = api.getCurrentDayFixtures();
        Gson gson = new Gson();

        return gson.fromJson(jsonStr, Fixture[].class);

    }

    @GetMapping(value = "/getFixtures")
    public Fixture[] getFixtures(@RequestParam() String startDate,
                                 @RequestParam() String endDate) throws IOException {

        Boolean isValid = dateValidator.validateDates(startDate, endDate);

        if(!isValid.equals(true)) {
        log.error("Invalid Format: Please ensure dates are in yyyy-MM-dd format");
        }

        String jsonStr = api.getFixtures(startDate, endDate);
        Gson gson = new Gson();

        return gson.fromJson(jsonStr, Fixture[].class);
    }


}
