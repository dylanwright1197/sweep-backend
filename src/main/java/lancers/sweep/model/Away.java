package lancers.sweep.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Away {

    public String time;
    public String substitution;
    public List<Lineup> starting_lineups;
    public List<Substitute> substitutes;
    public List<Coach> coach;
    public List<MissingPlayer> missing_players;
}
