package lancers.sweep.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoalScorer {
    public String time;
    public String home_scorer;
    public String home_scorer_id;
    public String home_assist;
    public String home_assist_id;
    public String score;
    public String away_scorer;
    public String away_scorer_id;
    public String away_assist;
    public String away_assist_id;
    public String info;
}
