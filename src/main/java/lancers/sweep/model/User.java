package lancers.sweep.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sweep_users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @JsonIgnore
    private ObjectId _id;
    private String username;
    private String password;
    private String roleId;
    private String status;


    public User(String username, String password, String roleId) {
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }

    public User(String username, String password, String roleId, String status) {
        this.username = username;
        this.password = password;
        this.roleId = roleId;
        this.status = status;
    }

}
