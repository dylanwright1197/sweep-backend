package lancers.sweep.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class League {
    public String leagueId;
    public String leagueName;
    public List<User> users;
    public String competition;
    public Leaderboard leaderBoard;
    public String admin;
    public String password;
}
