package lancers.sweep.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MissingPlayer {
    public String lineup_player;
    public String lineup_number;
    public String lineup_position;
    public String player_key;
}
