package lancers.sweep.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@Document(collection = "fixtures")
public class Fixture {

    public String match_id;
    public String country_id;
    public String country_name;
    public String league_id;
    public String league_name;
    public String match_date;
    public String match_status;
    public String match_time;
    public String match_hometeam_id;
    public String match_hometeam_name;
    public String match_hometeam_score;
    public String match_awayteam_name;
    public String match_awayteam_id;
    public String match_awayteam_score;
    public String match_hometeam_halftime_score;
    public String match_awayteam_halftime_score;
    public String match_hometeam_extra_score;
    public String match_awayteam_extra_score;
    public String match_hometeam_penalty_score;
    public String match_awayteam_penalty_score;
    public String match_hometeam_ft_score;
    public String match_awayteam_ft_score;
    public String match_hometeam_system;
    public String match_awayteam_system;
    public String match_live;
    public String match_round;
    public String match_stadium;
    public String match_referee;
    public String team_home_badge;
    public String team_away_badge;
    public String league_logo;
    public String country_logo;
    public List<GoalScorer> goalscorer;
    public List<Card> cards;
    public Substitute substitutions;
    public Lineup lineup;
    public List<Statistic> statistics;

}
