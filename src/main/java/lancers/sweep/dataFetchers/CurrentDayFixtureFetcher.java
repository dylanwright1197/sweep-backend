package lancers.sweep.dataFetchers;

import com.google.gson.Gson;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lancers.sweep.api.Api;
import lancers.sweep.model.Fixture;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CurrentDayFixtureFetcher implements DataFetcher {

    private Api api;


    @Autowired
    CurrentDayFixtureFetcher(Api api){
        this.api = api;
    }


    @SneakyThrows
    @Override
    public List<Fixture> get(DataFetchingEnvironment dataFetchingEnvironment) {

        String jsonStr = api.getCurrentDayFixtures();
        Gson gson = new Gson();

        Fixture[] fixture = gson.fromJson(jsonStr, Fixture[].class);
        return Arrays.asList(fixture);
    }


}
