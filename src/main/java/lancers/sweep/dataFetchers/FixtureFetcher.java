package lancers.sweep.dataFetchers;

import com.google.gson.Gson;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lancers.sweep.api.Api;
import lancers.sweep.model.Fixture;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class FixtureFetcher implements DataFetcher {

    private Api api;


    @Autowired
    FixtureFetcher(Api api){
        this.api = api;
    }


    @SneakyThrows
    @Override
    public List<Fixture> get(DataFetchingEnvironment dataFetchingEnvironment) {

        Map args = dataFetchingEnvironment.getArguments();

        String jsonStr = api.getFixtures(args.get("startDate").toString(), args.get("endDate").toString());
        Gson gson = new Gson();

        Fixture[] fixture = gson.fromJson(jsonStr, Fixture[].class);
        return Arrays.asList(fixture);
    }


}
