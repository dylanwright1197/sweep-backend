package lancers.sweep.repository;

import lancers.sweep.model.League;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LeagueRepository extends MongoRepository<League, String> {
}
