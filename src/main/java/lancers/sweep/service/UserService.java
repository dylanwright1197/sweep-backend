package lancers.sweep.service;


import lancers.sweep.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    void createUser(User user);
}
