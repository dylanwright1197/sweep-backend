package lancers.sweep.service.impl;


import lancers.sweep.model.User;
import lancers.sweep.repository.UserRepository;
import lancers.sweep.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();

    }


    @Override
    public void createUser(User user) {
        userRepository.save(user);

    }

}
