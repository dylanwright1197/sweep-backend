package lancers.sweep.service.impl;

import lancers.sweep.model.Role;
import lancers.sweep.model.User;
import lancers.sweep.repository.RoleRepository;
import lancers.sweep.repository.UserRepository;
import lancers.sweep.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {


    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Role getUserRole(String username) {
        User user = userRepository.findByUsername(username);
        String roleId = user.getRoleId();

        List<Role> roleList = roleRepository.findAll();

        for(Role r: roleList) {
            if (r.getRoleId().equals(roleId)) {
                return r;
            }
        }

        return null;

    }
}
