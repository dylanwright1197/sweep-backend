package lancers.sweep.service.impl;

import lancers.sweep.model.League;
import lancers.sweep.repository.LeagueRepository;
import lancers.sweep.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeagueServiceImpl implements LeagueService {

    @Autowired
    LeagueRepository leagueRepository;

    @Override
    public void createLeague(League league) {
        leagueRepository.save(league);
    }

    @Override
    public List<League> getAllLeagues() {
       return leagueRepository.findAll();
    }

    @Override
    public League getLeagueByName(String leagueName) {
        List<League> allLeagues = getAllLeagues();

        for(League l : allLeagues) {
            if(l.getLeagueName().equals(leagueName)) {
                return l;
            }
        }

        return null;
    }
}
