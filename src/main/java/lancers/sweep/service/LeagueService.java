package lancers.sweep.service;

import lancers.sweep.model.League;

import java.util.List;

public interface LeagueService {

    void createLeague(League league);

    List<League> getAllLeagues();

    League getLeagueByName(String leagueName);
}
