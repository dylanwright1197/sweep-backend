package lancers.sweep.service;


import lancers.sweep.model.Role;

public interface RoleService {

    Role getUserRole(String username);
}
